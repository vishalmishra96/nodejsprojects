var express = require('express');
var router = express.Router();

/* Authenticating id password. */
router.get('/', function(req, res) {
	if (req.session.loggedin) {
		// res.send('Welcome back, ' + req.session.username + '!');
		res.render('main', { title: 'HomeJS' });
	} else {
		res.send('Please login to view this page!');
	}
	// res.end('<a href="/">Logout</a>');
	
});

module.exports = router;