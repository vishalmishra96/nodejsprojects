module.exports = function(app) {
  var todoList = require('../../controller/appController');

  // todoList Routes
  app.route('/api/v1')
    .get(todoList.list_all_tasks)
    .post(todoList.create_a_task);
   
   app.route('/api/v1/:serial')
    .get(todoList.read_a_task)
    };