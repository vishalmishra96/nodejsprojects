var Task = require('../models/appModel.js');

exports.list_all_tasks = function(req, res) {
  Task.getAllTask(function(err, task) {

    console.log('controller')
    if (err)
      res.send(err);
      console.log('res', task);
    res.send(task);
  });
};


exports.create_a_task = function(req, res) {
  var new_task = new Task(req.body);
    
    Task.createTask(new_task, function(err, task) {
      
      if (err)
        res.send(err);
      res.json({"status":true});
    });
};


exports.read_a_task = function(req, res) {
  Task.getTaskById(req.params.serial, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};
