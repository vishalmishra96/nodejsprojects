# Restful Api

This project is a part of appanalytics-tracker to provide a restful api and login module for the appanalytics-tracker project.

### Prerequisites
```
Install Node.js
Install npm
install nodemon
```
## Usage
Please Make sure to change the routes/db.js file according to your database credentials.