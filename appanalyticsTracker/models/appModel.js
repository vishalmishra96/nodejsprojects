var sql = require('../routes/db');

//Task object constructor
var Task = function(task){
    this.ip = task.ip;
    this.location = task.location;
    this.open_count = task.open_count;
    this.device_info = JSON.stringify(task.device_info);
};
Task.createTask = function createUser(newTask, result) {    
        sql.query("INSERT INTO sdk_data set ?", newTask, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res.insertId);
                    result(null, res.insertId);
                }
            });           
};
Task.getTaskById = function createUser(taskId, result) {
        sql.query("Select * from sdk_data where serial = ? ", taskId, function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};
Task.getAllTask = function getAllTask(result) {
        sql.query("Select * from sdk_data", function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('sdk_data : ', res);  

                 result(null, res);
                }
            });   
};


module.exports= Task;