/**
 * Summary: Main entry point of the application
 */
const EXPRESS = require("express");

const APP = EXPRESS();

const CONFIG = require("./config");
const DEBUG = require("./lib/debugger")("serverJs");

let isRunning = false;
let isTaskStarted = false;

function runApp() {
  const { PORT } = process.env;
  if (!PORT) {
    DEBUG("PORT is not defined");
    process.exit(1);
  }
  isRunning = true;
  APP.listen(PORT, (error) => {
    if (!error) {
      isRunning = true;
      DEBUG("APP is running on", PORT);
    } else {
      DEBUG("Error while running the app", error);
    }
  });
}

function start() {
  if (!isRunning && !isTaskStarted) {
    isTaskStarted = true;
    CONFIG(APP)
    .then(runApp)
    .catch((error) => {
        DEBUG(error);
        process.exit(1);
    });
  } else {
    DEBUG("Application is already running:");
  }
}

if (require.main === module){
    start();
}