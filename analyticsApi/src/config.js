/**
 * Summary: Put dependency code in this file. By dependency I mean the code/config/connection that
 *  must get loaded before running the app
 */
const BODY_PARSER = require("body-parser");
const MONGOOSE = require("mongoose");
const DOTENV = require("dotenv");

const API_ROUTES = require("./api/routes");

MONGOOSE.connect('mongodb://localhost/analyticsapi');
var db = MONGOOSE.connection;

function addFormatter() {
  return new Promise((resolve) => {
    String.prototype.format = String.prototype.format
    || function temp(..._arguments) {
      let str = this.toString();
      if (arguments.length) {
        const t = typeof _arguments[0];
        let key;
        const args = (t === "string" || t === "number")
          ? Array.prototype.slice.call(_arguments)
          : _arguments[0];
        // eslint-disable-next-line guard-for-in
        for (key in args) {
          str = str.replace(new RegExp(`\\{${key}\\}`, "gi"), args[key]);
        }
      }
      return str;
    };
    resolve();
  });
}

function loadEnvironmet() {
  return new Promise((resolve, reject) => {
    const RESULT = DOTENV.config();
    if (RESULT.error) {
        reject(RESULT.error);
    } else {
      resolve(true);
    }
  });
}

function loadPreRouteMiddlewares(appInstance) {
  return new Promise((resolve) => {
    appInstance.use(BODY_PARSER.urlencoded({
      extended: false,
    }));
    appInstance.use(BODY_PARSER.json());
    resolve(true);
  });
}

function loadRouters(appInstance) {
  return new Promise((resolve) => {
    API_ROUTES(appInstance);
    resolve(true);
  });
}

function loadDependencies(appInstance) {
  return new Promise((resolve, reject) => {
      loadEnvironmet()
      .then(addFormatter)
      .then(loadPreRouteMiddlewares(appInstance))
      .then(loadRouters(appInstance))
      .then(resolve)
      .catch(reject);
  });
}

module.exports = loadDependencies;
