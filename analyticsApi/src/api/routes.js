/**
 * Summary: Initialize routes for API application
 */
const CONTROLLERS = require("./controllers");
const BASE_URL = "/api";

module.exports = function routes(app) {
  app.get(`${BASE_URL}/`, CONTROLLERS.home);
  app.get(`${BASE_URL}/v1`, CONTROLLERS.view);
  app.post(`${BASE_URL}/trackdevice`, CONTROLLERS.create);
};

