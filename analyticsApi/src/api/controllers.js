/**
 * Summary: Contains controller code for the APIs
 */
const PATH = require("path");

const DEBUG = require("../lib/debugger")("api-controllers");

const MODEL = require("./models");

function status(req, res) {
  res.sendStatus(200);
}

function home(req, res) {
    DEBUG("Got call for home");
    res.sendFile(PATH.join(`${__dirname}/../public/home.html`));
}

// Handle create actions
function create (req, res) {
  const DEVICEINFO = new MODEL.DEVICEINFO();
  DEVICEINFO.os = req.body.os;
  DEVICEINFO.country = req.body.country;
  DEVICEINFO.packagename = req.body.packagename;
  DEVICEINFO.deviceid = req.body.deviceid;
  DEVICEINFO.devicemode = req.body.devicemode;
  DEVICEINFO.offerid = req.body.offerid;
  DEVICEINFO.affiliateid = req.body.affiliateid;
  DEVICEINFO.channelid = req.body.channelid;
  // save the contact and check for errors
  DEVICEINFO.save(function (err) {
      if (err)
          res.json(err);
      res.json({
                message: 'New DeviceInfo created!',
                data: DEVICEINFO
            });
      });
};

//view created data
function view(req, res) {
  MODEL.get(function (err, DEVICEINFO) {
      if (err) {
          res.json({
              status: "error",
              message: err,
          });
      }
      res.json({
          status: "success",
          message: "DeviceInfo retrieved successfully",
          data: DEVICEINFO
      });
  });
};

module.exports = {
  home,
  view,
  create,
  status,
};
