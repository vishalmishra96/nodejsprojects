const MONGOOSE = require('mongoose');
// Setup schema
const DEVICEINFOSCHEMA = MONGOOSE.Schema({
    os: {
        type: String,
        required: true
    },
    country: {
        type: String,
        required: true
    },
    packagename: {
        type: String,
        required: true
    },
    deviceid: {
        type: String,
        required: true
    },
    devicemode: {
        type: Number,
        default: 1
    },
    offerid: {
        type: String,
        required: true
    },
    affiliateid: {
        type: String,
        required: true
    },
    channelid: {
        type: String,
        required: true
    }
});

const DEVICEAPPSCHEMA = MONGOOSE.Schema({
    deviceid: String,
    country: String,
    apps: [{
        app_name: String,
        installed_at: Date,
    }],
    created_at: Date,
    updated_at: {
        type: Date,
        default: Date.now
    }
});

const ANDROIDINSTALLSCHEMA = MONGOOSE.Schema({
    deviceid: String,
    packagename: String,
    country: String,
    devicemode: Number,
    offerid: String,
    affiliateid: String,
    channelid: String,
    created_at: {
        type: Date,
        default: Date.now
    }
});

const IOSINSTALLSCHEMA = MONGOOSE.Schema({
    deviceid: String,
    packagename: String,
    country: String,
    devicemode: Number,
    offerid: String,
    affiliateid: String,
    channelid: String,
    created_at: {
        type: Date,
        default: Date.now
    }
});

// Export Schema
const DEVICEINFO = MONGOOSE.model('deviceinfo', DEVICEINFOSCHEMA);
const DEVICEAPP = MONGOOSE.model('deviceapp', DEVICEAPPSCHEMA);
const ANDROIDINSTALL = MONGOOSE.model('androidinstall', ANDROIDINSTALLSCHEMA);
const IOSINSTALL = MONGOOSE.model('iosinstall', IOSINSTALLSCHEMA);

module.exports = {
    DEVICEINFO,
    DEVICEAPP,
    ANDROIDINSTALL,
    IOSINSTALL
};

module.exports.get = function (callback, limit) {
    DEVICEINFO.find(callback).limit(limit);
}