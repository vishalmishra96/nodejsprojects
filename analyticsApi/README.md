### GETTING STARTED ###
* Configuration
	- create environment variables
		`cp dev.env  .env`
* Dependencies
	- install depenedencies.
		`npm install`
* How to run tests
	- start app in dev mode.
		`npm run start-dev`
* Deployment instructions
	- add any new environment varibale to .env and dev.env file
	- for any new dependency you install, save it to package.json by --save flag while installing that dependency.
